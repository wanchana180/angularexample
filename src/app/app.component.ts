import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { isNull } from 'util';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'personalInfo';
  editField: string;
    personList: Array<any> = [
      { id: 11, firstName: 'John',lastName:"Wick", age: 36,career:"Gun Slinger", companyName: 'Mako',phoneNo:"0987654321"},
      { id: 11, firstName: 'John',lastName:"Wick", age: 36,career:"Gun Slinger", companyName: 'Mako',phoneNo:"0987654321"},
      { id: 11, firstName: 'John',lastName:"Wick", age: 36,career:"Gun Slinger", companyName: 'Mako',phoneNo:"0987654321"},
      { id: 11, firstName: 'John',lastName:"Wick", age: 36,career:"Gun Slinger", companyName: 'Mako',phoneNo:"0987654321"},
      { id: 11, firstName: 'John',lastName:"Wick", age: 36,career:"Gun Slinger", companyName: 'Mako',phoneNo:"0987654321"},
      {"_id":"5ed18b7e4e17e9371cb7f006","firstName":"Jonh","lastName":"Vick","age":"45","career":"Gunner","companyName":"Sectum","phoneNo":"908876","__v":0}
      
    ];
    awaitingPersonList: Array<any> = [
      { id: 11, firstName: 'John',lastName:"Wick", age: 36,career:"Gun Slinger", companyName: 'Mako',phoneNo:"0987654321"},
      { id: 11, firstName: 'John',lastName:"Wick", age: 36,career:"Gun Slinger", companyName: 'Mako',phoneNo:"0987654321"},
      { id: 11, firstName: 'John',lastName:"Wick", age: 36,career:"Gun Slinger", companyName: 'Mako',phoneNo:"0987654321"},
      { id: 11, firstName: 'John',lastName:"Wick", age: 36,career:"Gun Slinger", companyName: 'Mako',phoneNo:"0987654321"},
      { id: 11, firstName: 'John',lastName:"Wick", age: 36,career:"Gun Slinger", companyName: 'Mako',phoneNo:"0987654321"},
      { id: 11, firstName: 'John',lastName:"Wick", age: 36,career:"Gun Slinger", companyName: 'Mako',phoneNo:"0987654321"}
    ];

    personFromDb: Array<any> = []
    getAll(){
      this.personFromDb.push( this.http.get('http://localhost:3000/selectAll').subscribe(result=>{
      
    }));
      console.log(this.personFromDb)
    }
    //callback
    onObservableLoad(){
      this.http
    }
    

  constructor(private http:HttpClient){

  }
  updateList(id: number, property: string, event: any) {
    const editField = event.target.textContent;
    this.personList[id][property] = editField;
  }

  remove(id: any) {
    this.awaitingPersonList.push(this.personList[id]);
    this.personList.splice(id, 1);
  }

  edit(id: number) {
    
    this.http.post<any>('http://localhost:3000/createProfileList',this.personList[id]).subscribe(result=>{
      alert(JSON.stringify(result));
    });
    
  }


  Add(profileFroms: NgForm){
    if (profileFroms.value != false){
      if (this.awaitingPersonList.length > 0) {
        const person = profileFroms;
        this.personList.push(person);
      }

    }else{
      alert("Please fill all data");
    }
    
    this.http.post<any>('http://localhost:3000/createProfileList',profileFroms).subscribe(result=>{
      alert(JSON.stringify(result));
    });
    
  }
  changeValue(id: number, property: string, event: any) {
    this.editField = event.target.textContent;
  }


  
  ngOnInit(): void {
    throw new Error("Method not implemented.");
  }
}
