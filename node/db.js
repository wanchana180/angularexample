var mongoose = require('mongoose');


mongoose.connect('mongodb://127.0.0.1/testDb',{ useNewUrlParser: true })

mongoose.connection.on('connected',function(){
    console.log('connected to mongoDb...')

});

mongoose.connection.on('error',function(err){
    console.log('error to default mongoose' + err)

});

mongoose.connection.on('disconnected',function(){
    console.log('disconnected to default mongoose')

});

process.on('SIGINT', function() {
    mongoose.connection.close(function() {
        console.log('disconnect to app terminate');
        process.exit(0);
    });

});