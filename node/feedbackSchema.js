var mongoose = require('mongoose');

var feedbackSchema = mongoose.Schema({
    username: {type:String,  require: true},
    feedback: {type:String,  require: true}

})


var feedBackModel = mongoose.model('feedback',feedbackSchema);
module.exports = feedBackModel;
