var mongoose = require('mongoose');

var profileSchema = mongoose.Schema({
    id:         {type:String,  require: true},
    firstName:  {type:String,  require: true},
    lastName:   {type:String,  require: true},
    age:        {type:String,  require: true},
    career:     {type:String,  require: true},
    companyName: {type:String,  require: true},
    phoneNo:    {type:String,  require: true}

})


var profileModel = mongoose.model('profile',profileSchema);
module.exports = profileModel;