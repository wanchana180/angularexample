const express = require('express');
const bodyParser = require('body-parser');
const app = express();
require('./db');
const feedBackModel = require('./feedbackSchema');
const profileModel  = require('./profileSchema');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.use(function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Allow-Methods','GET,POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers','content-type', 'x-access-token');
    res.setHeader('Access-Control-Allow-Credentials',true);
    next();
});


app.get('/',(req, res)=>{
    res.end("to root")
});

app.get('/home',(req, res)=>{
    res.end("to home")
});

app.post('/api',(req, res)=>{
    const username = req.body.username;
    const feedback = req.body.feedback;
    

    feedBackModel.create(req.body, (err,doc)=>{
        if (err) res.json({result: "failed",username: username,feedback: feedback});
        res.json({result: "success to DB",username: username,feedback: feedback});
    })
    
    
});

app.get('/selectFrom',(req,res)=>{
    feedBackModel.find((err,doc)=>{
        if (err) res.json({result: 'failed'})
        res.json({result: 'success',data: doc})
    })
});
app.get('/selectFromProfileList',(req,res)=>{
    profileModel.findByIdAndUpdate({"req.id"},
    {"breed": "Great Dane"}, function(err, result){
        if(err){
            res.send(err)
        }
        else{
            res.send(result)
        }
    })
});

app.get("/selectAll",(req, res) =>{
    profileModel.find({}, function(err, result) {
      if (err) {
        console.log(err);
      } else {
        res.json(result);
      }
    });
  });


app.post('/createProfileList',(req, res)=>{
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    profileModel.create(req.body, (err,doc)=>{
        if (err) res.json({result: "failed",firstName: firstName,lastName: lastName});
        res.json({result: "Add Success",firstName: firstName,lastName: lastName});
    })
    
    
});

app.post('/EditProfileList',(req, res)=>{
    
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    profileModel.findOneAndUpdate(req.body, (err,doc)=>{
        if (err) res.json({result: "failed",firstName: firstName,lastName: lastName});
        res.json({result: "Profile Updated",firstName: firstName,lastName: lastName});
    })
    
    
});


app.listen(3000, ()=>{
    console.log("is running......");
})

